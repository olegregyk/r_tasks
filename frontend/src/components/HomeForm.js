import React from 'react';
import './global-style.css';
import TaskList from './TaskList';
import Header from "./Header";
const HomePage = () => {
    return (
        <div>
            <Header />
            <div>
                <br/><br/><br/>
                <TaskList />
            </div>
        </div>

    );
};

export default HomePage;

