import React, { useState } from 'react';
import axios from 'axios';
import Header from './Header';
const LoginPage = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [message, setMessage] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();

        const data = {
            email: email,
            password: password
        };

        axios.post('http://localhost:8000/api/login', data)
            .then(response => {
                setMessage(response.data.message);
                if (response.data.token) {
                    localStorage.setItem('jwtToken', response.data.token);
                    localStorage.setItem('login', email);
                    localStorage.setItem('userId', response.data.user_id);
                    console.log('Токен збережено в localStorage:', response.data.token, response.data.user_id);
                    window.location.href = '/';
                } else {
                    console.log('Помилка отримання токену:', response.data.message);
                }
            })
            .catch(error => {
                setMessage('Помилка');
                console.error(error);
            });
    };

    return (
        <div>
            <Header />
            <br/><br/><br/>
            <h2>Форма авторизації</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Login:</label>
                    <input type="text" value={email} onChange={event => setEmail(event.target.value)} required />
                </div>
                <div>
                    <label>Password:</label>
                    <input type="password" value={password} onChange={event => setPassword(event.target.value)} required />
                </div>
                <button type="submit">Вхід</button>
            </form>
            {message && <p>{message}</p>}
        </div>
    );
};





export default LoginPage;