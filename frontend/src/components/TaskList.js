import React, { useState, useEffect } from 'react';
import axios from 'axios';

const TaskList = () => {
    const [tasks, setTasks] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [sortBy, setSortBy] = useState('');
    const [sortOrder, setSortOrder] = useState('');
    const [statusFilter, setStatusFilter] = useState('');
    const [priorityMin, setPriorityMin] = useState('');
    const [priorityMax, setPriorityMax] = useState('');

    const fetchTasks = async () => {
        try {
            const queryParams = {};
            if (searchTerm) queryParams.search = searchTerm;
            if (sortBy) queryParams.sortBy = sortBy;
            if (sortOrder) queryParams.sortOrder = sortOrder;
            if (statusFilter) queryParams.status = statusFilter;
            if (priorityMin) queryParams.priorityMin = priorityMin;
            if (priorityMax) queryParams.priorityMax = priorityMax;

            const response = await axios.get(`http://localhost:8000/api/tasks`, {
                params: queryParams,
            });
            setTasks(response.data);
        } catch (error) {
            console.error('Error fetching tasks:', error);
        }
    };

    useEffect(() => {
        fetchTasks();
    }, [searchTerm, sortBy, sortOrder, statusFilter, priorityMin, priorityMax]);

    const handleFilterClick = () => {
        fetchTasks();
    };

    const fetchAllTasks = async () => {
        try {
            const response = await axios.get(`http://localhost:8000/api/tasks`);
            setTasks(response.data);
        } catch (error) {
            console.error('Error fetching tasks:', error);
        }
    };


    return (
        <div>
            <div>
                <input
                    type="text"
                    placeholder="Пошук за назвою або описом"
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                />
                <select value={sortBy} onChange={(e) => setSortBy(e.target.value)}>
                    <option value="">Все</option>
                    <option value="createdAt">Час створення</option>
                    <option value="completedAt">Час виконання</option>
                    <option value="priority">Пріоритет</option>
                </select>
                <select value={sortOrder} onChange={(e) => setSortOrder(e.target.value)}>
                    <option value="">Все</option>
                    <option value="asc">Зростання</option>
                    <option value="desc">Спадання</option>
                </select>
                <select value={statusFilter} onChange={(e) => setStatusFilter(e.target.value)}>
                    <option value="">Всі статуси</option>
                    <option value="todo">Todo</option>
                    <option value="done">Done</option>
                </select>
                <input
                    type="number"
                    placeholder="Мінімальний пріоритет"
                    value={priorityMin}
                    onChange={(e) => setPriorityMin(e.target.value)}
                />
                <input
                    type="number"
                    placeholder="Максимальний пріоритет"
                    value={priorityMax}
                    onChange={(e) => setPriorityMax(e.target.value)}
                />
                <button onClick={() => {
                    setSearchTerm('');
                    setSortBy('');
                    setSortOrder('');
                    setStatusFilter('');
                    setPriorityMin('');
                    setPriorityMax('');
                }}>Скинути фільтри</button>

            </div>
            <h1>Список завдань</h1>
            <table>
                <thead>
                </thead>
                <tbody>
                {tasks.map(task => (
                    <tr key={task.id}>
                        <td>
                            <a href={`/tasks/${task.id}`}>{task.id}</a>
                        </td>
                        <td>{task.status}</td>
                        <td> {task.priority} </td>
                        <td>{task.title}</td>
                        <td>{task.description}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    );
};

export default TaskList;