import React from 'react';
const token = localStorage.getItem('jwtToken');
const login = localStorage.getItem('login');
const handleLogout = () => {
    localStorage.removeItem('jwtToken');
    window.location.href = '/';
};
const Header = () => {
    if(token){
        return (
            <header>
                <div className="header-buttons">
                    <a href="/">На головну</a>
                    <a href="/create">Створити завдання</a>
                    <a onClick={handleLogout} href={'/'}>Вийти</a>
                    <h3>Login:{login}</h3>
                </div>
            </header>
        );
    }
    else
    {
        return (
            <header>
                <div className="header-buttons">
                    <a href="/">На головну</a>
                    <a href="/login">Вхід</a>
                    <a href="/registration">Реєстрація</a>
                </div>
            </header>

        );
    }

};

export default Header;
