import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from "./Header";

const TaskDetails = ({ match  }) => {
    const taskId = match.params.id;
    const [task, setTask] = useState(null);
    const [newTitle, setNewTitle] = useState('');
    const [newDescription, setNewDescription] = useState('');
    const [newStatus, setNewStatus] = useState('');
    const [newPriority, setNewPriority] = useState('');
    const [subtasks, setSubtasks] = useState([]);
    const [isChangesSaved, setIsChangesSaved] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [CreatedByUserId, setCreatedByUserId] = useState('');
    const userId = localStorage.getItem('userId');
    useEffect(() => {
        const fetchTaskDetails = async () => {
            try {
                const response = await axios.get(`http://localhost:8000/api/tasks/${taskId}`);
                setTask(response.data);
                setNewTitle(response.data.title);
                setNewDescription(response.data.description);
                setNewStatus(response.data.status);
                setNewPriority(response.data.priority);
                setCreatedByUserId(response.data.creator_user_id);
                const subtasksResponse = await axios.get(`http://localhost:8000/api/tasks/${taskId}/subtasks`);
                setSubtasks(subtasksResponse.data);
            } catch (error) {
                console.error('Error fetching task details:', error);
            }
        };
        fetchTaskDetails();
    }, [taskId]);

    const handleStatusChange = async (event) => {
        const newStatus = event.target.value;

        if (newStatus === 'done') {
            const hasUncompletedSubtasks = subtasks.some(subtask => subtask.status !== 'done');

            if (hasUncompletedSubtasks) {
                alert('Неможливо встановити статус "done", у цього завдання є невиконані підзавдання. Спочатку виконайте всі підзавдання.');
                return;
            }
        }

        setNewStatus(newStatus);
    };

    const handleTitleChange = (event) => {
        setNewTitle(event.target.value);
    };

    const handleDescriptionChange = (event) => {
        setNewDescription(event.target.value);
    };
    const handlePriorityChange = (event) => {
        setNewPriority(event.target.value);
    }

    const handleSaveChanges = async () => {
        try {
            await axios.put(`http://localhost:8000/api/tasks/${taskId}`, {
                title: newTitle,
                description: newDescription,
                status: newStatus,
                priority: newPriority,
            });

            const response = await axios.get(`http://localhost:8000/api/tasks/${taskId}`);
            setTask(response.data);
            setIsChangesSaved(true);
        } catch (error) {
            console.error('Error saving changes:', error);
            setErrorMessage(error.response.data.error);
        }
    };
    const handleDeleteTask = async () => {
        try {
            await axios.delete(`http://localhost:8000/api/tasks/${taskId}`);
            window.location.href = ('/');
        } catch (error) {
            console.error('Error deleting task:', error);
            setErrorMessage(error.response.data.error);
        }
    };

    if (!task) {
        return <div>Loading...</div>;
    }
    const isOwner = CreatedByUserId == userId;
    return (
        <div>
            <Header /><br /><br /><br />
            <h1>Деталі завдання</h1>
            <p>ID: {task.id}</p>
            <select value={newStatus} onChange={handleStatusChange} disabled={task.status === 'done'}>
                <option value="todo">todo</option>
                <option value="done">done</option>
            </select><br/>
            <label>Пріоритет </label>
            <select value={newPriority} onChange={handlePriorityChange} disabled={!isOwner || task.status === 'done'}>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <input
                type="text"
                value={newTitle}
                onChange={handleTitleChange}
                disabled={!isOwner || task.status === 'done'}
            />
            <textarea
                value={newDescription}
                onChange={handleDescriptionChange}
                className="description"
                disabled={!isOwner || task.status === 'done'}
            />
            {isChangesSaved && <p>Зміни успішно збережені!</p>}
            {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
            {task.status === 'done' ? (
                <p>Завдання виконане!</p>
            ) : (
                <div>
                    <button onClick={handleSaveChanges}>Зберегти зміни</button>

                {isOwner && (
                    <div>
                        <a href={`/create/?parent_task_id=${task.id}`}>Створити підзавдання</a>
                        <button onClick={handleDeleteTask}>Видалити завдання</button>
                    </div>
                )}</div>
            )}

            {subtasks.length > 0 ? (
                <ul>
                    {subtasks.map(subtask => (
                        <td key={subtask.id}>
                            <a href={`/tasks/${subtask.id}`}>{subtask.title}</a>
                            <p>{subtask.description}</p>
                        </td>
                    ))}
                </ul>
            ) : (
                <p>Підзавдань немає.</p>
            )}
        </div>
    );
};

export default TaskDetails;