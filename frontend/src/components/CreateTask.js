import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useHistory, useLocation } from 'react-router-dom';
import Header from './Header';
const CreateTask = () => {
    const history = useHistory();
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    const parentTaskId = queryParams.get('parent_task_id');
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [priority, setPriority] = useState(1);
    const userId = localStorage.getItem('userId');
    const [successMessage, setSuccessMessage] = useState('');
    const handleCreateTask = async () => {
        try {
            const response = await axios.post('http://localhost:8000/api/tasks/create', {
                user_id: userId,
                title: title,
                description: description,
                priority: priority,
                parent_task_id: parentTaskId
            });
            setSuccessMessage('Завдання було успішно додане.');
        } catch (error) {
            console.error('Error creating task:', error);
        }
    };
    const createTaskURL = parentTaskId ? `http://localhost:3000/tasks/${parentTaskId}` : 'http://localhost:3000/';
    return (
        <div>
            <a href={'/'}>На головну</a> <br></br><br></br>
            <h1>Створення завдання</h1>
            <form>
                <label>Заголовок</label>
                <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />

                <label>Опис</label><br></br>
                <textarea value={description} onChange={(e) => setDescription(e.target.value)} />
                <br></br>
                <label>Пріоритет  </label>
                <select value={priority} onChange={(e) => setPriority(e.target.value)}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                <a type="button" href={createTaskURL} onClick={handleCreateTask}>Створити завдання</a>
                {successMessage && <p>{successMessage}</p>}
            </form>
        </div>
    );
};

export default CreateTask;