import React, { useState } from 'react';
import axios from 'axios';
import Header from './Header';
const RegistrationPage = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordCheck, setPasswordCheck] = useState('');
    const [message, setMessage] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        if (password!== passwordCheck)
        {
            setMessage('Паролі не співпадають');
            return;
        }
        const data = {
            email: email,
            password: password
        };

        axios.post('http://localhost:8000/api/register', data)
            .then(response => {
                setMessage(response.data.message);
                window.location.href = '/login';
            })
            .catch(error => {
                setMessage('Помилка');
                console.error(error);
            });
    };

    return (
        <div>
            <Header />
            <br/><br/><br/>
            <h2>Форма реєстрації</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Login:</label>
                    <input type="text" value={email} onChange={event => setEmail(event.target.value)} required />
                </div>
                <div>
                    <label>Password:</label>
                    <input type="password" value={password} onChange={event => setPassword(event.target.value)} required /><br/>
                    <input type="password" value={passwordCheck} onChange={event => setPasswordCheck(event.target.value)} required />
                </div>
                <button type="submit">Зареєструватися</button>
            </form>
            {message && <p>{message}</p>}
        </div>
    );
};

export default RegistrationPage;