import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import RegistrationPage from './components/RegistrationForm';
import LoginPage from './components/LoginForm';
import HomePage from './components/HomeForm';
import CreateTask from "./components/CreateTask";
import TaskDetails from './components//TaskDetails';
const App = () => {
  return (
      <Router>
        <Switch>
            <Route path="/registration" exact component={RegistrationPage} />
            <Route path="/login" exact component={LoginPage} />
            <Route path="/" exact component={HomePage} />
            <Route path="/create" exact component={CreateTask} />
            <Route path="/tasks/:id" component={TaskDetails} />
        </Switch>
      </Router>
  );
};

export default App;







