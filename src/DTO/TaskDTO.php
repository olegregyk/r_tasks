<?php
namespace App\DTO;

class TaskDTO
{
    public ?int $id = null;
    public string $status = '';
    public int $priority = 1;
    public string $title = '';
    public string $description = '';
    public ?int $creator_user_id = null;
    public ?array $subTasks = null;
}