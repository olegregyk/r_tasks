<?php
namespace App\DTO;
class SubtaskDTO
{
    public int $id;
    public string $title;
    public string $status;
    public int $priority;
    public string $description;
    public \DateTimeInterface $createdAt;
    public ?\DateTimeInterface $completedAt;

    public function __construct(
        int $id,
        string $title,
        string $status,
        int $priority,
        string $description,
        \DateTimeInterface $createdAt,
        ?\DateTimeInterface $completedAt
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->status = $status;
        $this->priority = $priority;
        $this->description = $description;
        $this->createdAt = $createdAt;
        $this->completedAt = $completedAt;
    }
}