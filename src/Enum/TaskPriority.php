<?php
namespace App\Enum;

class TaskPriority
{
    const PRIORITY_1 = 1;
    const PRIORITY_2 = 2;
    const PRIORITY_3 = 3;
    const PRIORITY_4 = 4;
    const PRIORITY_5 = 5;

    public static function getAll(): array
    {
        return [
            self::PRIORITY_1 => 'Низький',
            self::PRIORITY_2 => 'Середній',
            self::PRIORITY_3 => 'Середній+',
            self::PRIORITY_4 => 'Високий',
            self::PRIORITY_5 => 'Надзвичайно високий',
        ];
    }
}