<?php
namespace App\Enum;

class TaskStatus
{
    const TODO = 'todo';
    const IN_PROGRESS = 'in_progress';
    const DONE = 'done';
}