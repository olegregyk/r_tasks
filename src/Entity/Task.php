<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Enum\TaskStatus;
use App\Enum\TaskPriority;
#[ORM\Entity(repositoryClass: TaskRepository::class)]
class Task
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups("user_details")]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups("user_details")]
    private $status = TaskStatus::TODO;

    #[ORM\Column(type: "integer")]
    #[Groups("user_details")]
    private $priority = TaskPriority::PRIORITY_1;

    #[ORM\Column(length: 255)]
    #[Groups("user_details")]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups("user_details")]
    private ?string $description = null;

    #[ORM\ManyToOne(targetEntity:'User', inversedBy:'tasks')]
    #[ORM\JoinColumn(nullable:false)]
    private $user;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups("user_details")]
    private ?\DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups("user_details")]
    private ?\DateTimeInterface $completedAt = null;

    #[ORM\ManyToOne(targetEntity: Task::class, inversedBy: 'subTasks')]
    private $parentTask;

    #[ORM\OneToMany(targetEntity: Task::class, mappedBy: 'parentTask')]
    private $subTasks;
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->subTasks = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): static
    {
        $this->priority = $priority;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function setUser(UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCompletedAt(): ?\DateTimeInterface
    {
        return $this->completedAt;
    }

    public function setCompletedAt(?\DateTimeInterface $completedAt): static
    {
        $this->completedAt = $completedAt;

        return $this;
    }
    public function addSubTask(Task $subTask): self
    {
        if (!$this->subTasks->contains($subTask)) {
            $this->subTasks[] = $subTask;
            $subTask->setParentTask($this);
        }

        return $this;
    }

    public function removeSubTask(Task $subTask): self
    {
        if ($this->subTasks->removeElement($subTask) && $subTask->getParentTask() === $this) {
            $subTask->setParentTask(null);
        }

        return $this;
    }

    public function getParentTask(): ?self
    {
        return $this->parentTask;
    }

    public function setParentTask(?self $parentTask): self
    {
        $this->parentTask = $parentTask;

        return $this;
    }

}
