<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Task>
 *
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }
    public function findTasksWithFilters(
        string $searchTerm,
        string $sortBy,
        string $sortOrder,
        string $statusFilter,
        int $priorityMin,
        int $priorityMax
    ): array {
        $queryBuilder = $this->createQueryBuilder('t');

        if (!empty($searchTerm)) {
            $queryBuilder
                ->andWhere('t.title LIKE :searchTerm OR t.description LIKE :searchTerm')
                ->setParameter('searchTerm', '%' . $searchTerm . '%');
        }

        if ($statusFilter === 'todo' || $statusFilter === 'done') {
            $queryBuilder
                ->andWhere('t.status = :statusFilter')
                ->setParameter('statusFilter', $statusFilter);
        } elseif ($statusFilter === 'empty') {
            $queryBuilder
                ->andWhere('t.status IS NULL');
        }

        $queryBuilder
            ->andWhere('t.priority >= :priorityMin AND t.priority <= :priorityMax')
            ->setParameter('priorityMin', $priorityMin)
            ->setParameter('priorityMax', $priorityMax);

        if (!empty($sortBy) && ($sortBy === 'createdAt' || $sortBy === 'completedAt' || $sortBy === 'priority')) {
            $queryBuilder->orderBy('t.' . $sortBy, $sortOrder);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
