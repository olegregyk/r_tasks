<?php
namespace App\Service;

use App\Entity\Task;
use App\Entity\User;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserRepository;
class TaskService
{
    private $userRepository;
    private TaskRepository $taskRepository;
    private EntityManagerInterface $entityManager;


    public function __construct(TaskRepository $taskRepository, EntityManagerInterface $entityManager,UserRepository $userRepository)
    {
        $this->taskRepository = $taskRepository;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function getTasks(): array
    {
        return $this->taskRepository->findAll();
    }
    public function getSubtasks(int $taskId): array
    {
        return $this->taskRepository->findBy(['parentTask' => $taskId]);
    }
    public function searchTasks(string $query): array
    {
        return $this->taskRepository->searchTasks($query);
    }
    public function getTaskById(int $taskId): ?Task
    {
        return $this->taskRepository->find($taskId);
    }

    public function createTask(array $data): Task
    {
        $user = $this->userRepository->find($data['user_id']);
        if (!$user) {
        throw new \InvalidArgumentException('Користувача не знайдено');
        }

        $task = new Task();
        if(isset($data['parent_task_id'])){
            $parentTask = $this->taskRepository->find($data['parent_task_id']);
            $task->setParentTask($parentTask);
        }
        $task->setUser($user);
        $task->setStatus("todo");
        if (isset($data['priority'])) {
            $task->setPriority($data['priority']);
        } else {
            $task->setPriority("1");
        }
        $task->setTitle($data['title']);
        $task->setDescription($data['description']);


        $this->entityManager->persist($task);
        $this->entityManager->flush();

        return $task;
    }

    public function updateTask(int $id, array $data): Task
    {
        $task = $this->taskRepository->find($id);
        if (!$task) {
            throw new \InvalidArgumentException("Задачі з таким id не існує $id");
        }
        if (isset($data['status']) && $data['status'] === 'done') {
            $task->setStatus($data['status']);
            $this->completeTask($id);
        }
        if (isset($data['priority'])) {
            $task->setPriority($data['priority']);
        }
        if (isset($data['title'])) {
            $task->setTitle($data['title']);
        }
        if (isset($data['description'])) {
            $task->setDescription($data['description']);
        }

        $this->entityManager->flush();

        return $task;
    }

    public function deleteTask(int $id): void
    {
        $task = $this->taskRepository->find($id);
        $status = $task->getStatus();
        if ($task and $status!="done") {
            $this->entityManager->remove($task);
            $this->entityManager->flush();
        }
        else
        {
            throw new \InvalidArgumentException("Задача вже виконана");
        }
    }

    public function completeTask(int $id): Task
    {
        $task = $this->taskRepository->find($id);
        if (!$task) {
            throw new \InvalidArgumentException("Задачі з таким id не існує $id");
        }

        $task->setCompletedAt(new \DateTime());
        $task->setStatus("done");
        $this->entityManager->flush();

        return $task;
    }
    public function hasUncompletedSubtasksRecursive(int $taskId): bool
    {
        $task = $this->taskRepository->find($taskId);

        if (!$task) {
            return false;
        }

        $subtasks = $this->taskRepository->findBy(['parentTask' => $taskId]);

        foreach ($subtasks as $subtask) {
            if ($subtask->getStatus() !== 'done') {
                return true;
            }

            if ($this->hasUncompletedSubtasksRecursive($subtask->getId())) {
                return true;
            }
        }

        return false;
    }

}