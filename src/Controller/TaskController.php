<?php

namespace App\Controller;

use App\DTO\SubtaskDTO;
use App\DTO\TaskDTO;
use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Service\TaskService;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;


#[Route('/api/tasks', name: 'api_task')]
class TaskController extends AbstractController
{
    private TaskService $taskService;
    private SerializerInterface $serializer;

    public function __construct(TaskService $taskService,SerializerInterface $serializer)
    {
        $this->taskService = $taskService;
        $this->serializer = $serializer;
    }

    #[Route('/', methods:['GET'])]
    public function listTasks(Request $request, TaskRepository $taskRepository, SerializerInterface $serializer): JsonResponse {
        $searchTerm = $request->query->get('search', '');
        $sortBy = $request->query->get('sortBy', 'createdAt');
        $sortOrder = $request->query->get('sortOrder', 'desc');
        $statusFilter = $request->query->get('status', '');
        $priorityMin = $request->query->getInt('priorityMin', 0);
        $priorityMax = $request->query->getInt('priorityMax', PHP_INT_MAX);

        $tasks = $taskRepository->findTasksWithFilters(
            $searchTerm,
            $sortBy,
            $sortOrder,
            $statusFilter,
            $priorityMin,
            $priorityMax
        );

        $serializedTasks = $serializer->normalize($tasks, null, ['groups' => 'user_details']);

        return new JsonResponse($serializedTasks);
    }


    #[Route('/{id}', name: 'api_task_get', methods: ['GET'])]
    public function getTask(int $id): JsonResponse
    {
        $task = $this->taskService->getTaskById($id);

        if (!$task) {
            return new JsonResponse(['error' => 'Задачу не знайдено'], 404);
        }

        $taskDTO = new TaskDTO();
        $taskDTO->id = $task->getId();
        $taskDTO->status = $task->getStatus();
        $taskDTO->priority = $task->getPriority();
        $taskDTO->title = $task->getTitle();
        $taskDTO->description = $task->getDescription();

        $user = $task->getUser();
        $creatorUserId = $user->getId();

        $taskDTO->creator_user_id = $creatorUserId;

        return $this->json($taskDTO);
    }
    #[Route('/{id}/subtasks', methods: ['GET'])]
    public function getSubtasks(int $id): JsonResponse
    {
        $subtasks = $this->taskService->getSubtasks($id);

        $subtaskDTOs = [];
        foreach ($subtasks as $subtask) {
            $subtaskDTO = new SubtaskDTO(
                $subtask->getId(),
                $subtask->getTitle(),
                $subtask->getStatus(),
                $subtask->getPriority(),
                $subtask->getDescription(),
                $subtask->getCreatedAt(),
                $subtask->getCompletedAt()
            );
            $subtaskDTOs[] = $subtaskDTO;
        }

        return $this->json($subtaskDTOs);
    }
    #[Route('/create', methods:['POST'])]
    public function createTask(Request $request, TaskService $taskService, SerializerInterface $serializer): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        try {
            $task = $taskService->createTask($data);
        } catch (\InvalidArgumentException $e) {
            return new JsonResponse(['error' => $e->getMessage()], 400);
        }

        $serializedTask = $serializer->normalize($task, null, ['groups' => 'user_details']);

        return new JsonResponse($serializedTask);
    }
    #[Route('/search', methods: ['GET'])]
    public function searchTasks(Request $request, SerializerInterface $serializer): JsonResponse
    {
        $query = $request->query->get('query');
        $tasks = $this->taskService->searchTasks($query);

        $serializedTasks = $serializer->normalize($tasks, null, ['groups' => 'user_details']);

        return new JsonResponse($serializedTasks);
    }
    #[Route('/{id}', methods:['PUT'])]
    public function updateTask(Request $request, int $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $task = $this->taskService->getTaskById($id);

        if (isset($data['status']) && $data['status'] === 'done') {
            $hasUncompletedSubtasks = $this->taskService->hasUncompletedSubtasksRecursive($id);

            if ($hasUncompletedSubtasks) {
                return new JsonResponse(['error' => 'Завдання має невиконані підзавдання'], 400);
            }
        }

        if (!$task) {
            return new JsonResponse(['error' => 'Завдання не знайдено'], 404);
        }

        if ($task->getStatus() === 'done') {
            return new JsonResponse(['error' => 'Завдання вже виконане. Зміни недоступні.'], 400);
        }

        $updatedTask = $this->taskService->updateTask($id, $data);
        return $this->json($updatedTask, 200, [], ['groups' => 'user_details']);
    }

    #[Route('/{id}', methods:['DELETE'])]
    public function deleteTask(int $id): JsonResponse
    {
        $task = $this->taskService->getTaskById($id);
        $hasUncompletedSubtasks = $this->taskService->hasUncompletedSubtasksRecursive($id);

        if ($hasUncompletedSubtasks) {
            return new JsonResponse(['error' => 'Завдання має невиконані підзавдання'], 400);
        }
        if ($task->getStatus() === 'done') {
            return new JsonResponse(['error' => 'Завдання вже виконане. Зміни недоступні.'], 400);
        }
        $this->taskService->deleteTask($id);
        return $this->json(['message' => 'Завдання успішно видалене!']);
    }

    #[Route('/{id}', methods:['POST'])]
    public function completeTask(int $id): JsonResponse
    {
        $completedTask = $this->taskService->completeTask($id);
        return $this->json($completedTask, 200, [], ['groups' => 'user_details']);
    }

}
